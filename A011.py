# Grupo 11 Daniel Serafim nº 89428      Daniel Matos nº 89429

import math
import pickle
import time
from heapq import *
from itertools import permutations
from functools import reduce

class SearchProblem:
	def __init__(self, goal, model, auxheur = []):
		self.graph = model
		self.goal = goal

	def search(self, init, limitexp = 2000, limitdepth = 10, tickets = [math.inf,math.inf,math.inf], anyorder = False):
		self.heap = []
		self.depth = limitdepth
		self.tickets = tickets
		self.states  = {}
		self.expansions = 0
		self.heuristics = []
		self.indexes = []

		# Calculate heuristic
		index = []
		for i in range(len(init)):
			self.heuristics.append(self.BFS(init[i], self.goal[i]))
			index.append(i)

		if anyorder:
			self.indexes = [i for i in permutations(index, len(self.goal))]
			self.goal = [i for i in permutations(self.goal, len(self.goal))]
		else:
			self.indexes = [index]
			self.goal = [tuple(self.goal)]

		# Initialize heap
		self.heap.append(State([init, []], None, 0, tickets.copy(), self.heurCalc(init)))

		# Generate states
		while not self.inGoal() and self.expansions < limitexp:
			self.expand(heappop(self.heap))
			self.expansions += 1

		print(self.expansions)

		result = []
		positions = self.heap[0]

		while not self.inInit(init, positions):
			result.insert(0, positions.toList())
			positions = positions.parent

		result.insert(0, [[], init.copy()])

		return result

	def expand(self, positions):
		gen = [[(), ()]]
		tickets = positions.tickets

		# Generate states
		for i in positions.pos:
			tmp = gen
			gen = []
			while(len(tmp)):
				onGen = tuple(tmp.pop(0))

				for path in self.graph[i]:
					if tickets[path[0]] == 0 or path[1] in onGen[0]:
						continue
					gen.append([onGen[0] + tuple([path[1]]), onGen[1] + tuple([path[0]])])

		for state in gen:
			ticks = tickets.copy()
			skip = False
			for i in state[1]:
				if ticks[i] == 0:
						skip = True
				ticks[i] -= 1
			if (positions.depth == self.depth-1 and not self.isGoal(state)) or skip:
				continue

			heur = self.heurCalc(state[0])

			if heur > self.depth + 1:
				continue

			new = State(state, positions, positions.depth + 1, ticks, heur)
			stateHash = new.hash

			if stateHash in self.states.keys():
				del new
			else:
				self.states[stateHash] = True
				heappush(self.heap, new)

	def inGoal(self):
		return self.heap[0].pos in self.goal

	def inInit(self, init, positions):
		return list(positions.pos) == init

	def isGoal(self, positions):
		return positions[0] in self.goal

	def BFS(self, init, goal):
		toExpand = [goal]
		result = {goal: 0}

		while len(toExpand) > 0:
			vert = toExpand.pop(0)
			for path in self.graph[vert]:
				if path[1] not in result.keys():
					result[path[1]] = result[vert] + 1
					toExpand.append(path[1])

		return result

	def heurCalc(self, positions):
		maxH = math.inf
		bestAvg = 0
		for perm in self.indexes:
			avg = 0
			tmp = 0
			for i in range(len(positions)):
				if self.heuristics[perm[i]][positions[i]] > tmp:
					tmp = self.heuristics[perm[i]][positions[i]]
				avg += self.heuristics[perm[i]][positions[i]]
			if tmp < maxH:
				maxH = tmp
				bestAvg = avg / (len(positions) << 9)
		return maxH + bestAvg

	def chooseGoal(self, minH, i, length, lstInd, lstHeur):
		if i == length:
			return lstInd
		elif minH not in lstHeur[i]:
			return []

		ind = lstHeur[i].index(minH)
		if ind in lstInd:
			n = lstHeur.copy()
			n[i][ind] = math.inf

			return self.chooseGoal(minH, i, length, lstInd, n)
		else:
			lstInd.append(ind)
			return self.chooseGoal(minH, i + 1, length, lstInd, lstHeur)

	def __repr__(self):
		return str(self.heap)

class State:
	def __init__(self, positions, parent, depth, tickets, heur):
		self.pos = positions[0]
		self.parent = parent
		self.depth = depth
		self.cost = heur + depth
		self.trans = positions[1]
		self.tickets = tickets

	def __lt__(self, other):
		return self.cost < other.cost

	def __repr__(self):
		return str(self.pos) + " " + str(self.cost)

	def toList(self):
		return [list(self.trans), list(self.pos)]

	def hash(self):
		return self.pos + self.tickets
